Lecturer: Bu Suci
=================

## Kriteria Desain
Kriteria | Deskripsi | Bad example
-------- | --------- | -----------
Functionality | Apa fungsi/goal dari website tercapai dengan baik? | Contoh buruk: Ads yang menutupi fungsi utama
Interface | Apa desain tersebut menarik dan enak di mata? Intuitif? | Contoh buruk: Warna yang buruk, placement advertisement yang buruk
Usability | Performa yang baik, ease-of-use, delay yang kecil, gampang diingat | Contoh buruk: Advertisement yang banyak, popup memakan cpu

****

## Apa yang harus didiskusikan sebelum design
* Untuk siapa (user)
    * Dari umur
    * Kepentingan
    * Jenis kelamin
    * Suku dan ras
    * Daerah asal (Indo, Eropa, Amerika, Desa, Kota, dll.)
    * Keseharian pengguna
* Untuk apa (functionality)
    * Masalah user
    * Apa yang user ingin capai
    * JANGAN PERNAH bertanya keinginan dari user, kecual memang BENAR BENAR BENAR BENAR dibutuhkan informasinya
* Menggunakan apa penggunanya (platform)
    * Di mana
        * Daerah iklim
    * Bagaimana
    * Cara penggunaannya

****

## Interaction Design
> The design of spaces for human communication and interaction