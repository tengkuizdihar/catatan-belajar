Lecturer: Ibu Eva (TUMBLING DOWN TUMBLING DOWN TUMBLING DOWN)
=============================================================

## Intro
* Memperkenalkan teknologi pengembangan aplikasi enterprise "terkini".
* Ada konsep pengembangan aplikasi, framework pengembangan, penggunaan database, dan studi kasus.
* 2 SKS kelas, 2 SKS lab
* Bakal banyak banget praktik pemrogramannya
* Materi luar kelas juga harus aktif dicari
* **JANGAN TERLAMBAT SESI LAB!** 

***

## Sylabus thing
_______________________________________________
| Evaluasi        | Frekuensi     | Bobot     |
| --------        | :---------:   | :-----:   |
| Latihan         | 11            | 22        |
| Kuis            | 2             | 10        |
| Tugas individu  | 1             | 10        |
| UTS             | 1             | 25        |
| Final Group Proj| 1             | 30        |
| Partisipasi     |               | 3         |
| **Total**       | 16            | 100       |


***

## Pelajaran

### Enterprise Application
* Contohnya adalah SAP, dan modul-modulnya. 
* Includes records, private data, ERP, analysis result, automated billing system
* Doesn't include word processor, telephone switches, OS, compilers, games, etc.

### Building Enterprise Application
* It's hard, exponentially dependent on how complex it is.
* Need to support concurrency
* Performance is NUMBER ONE priority
    * Diukur dengan response timenya (bisa aja menjadi requirement)
* Jenis requirement
    * Functional
        * Security
    * Non-functional
        * Response time
        * Responsiveness
* Responsiveness
    * Seberapa cepat sistem memberikan RESPONS terhadap sebuah kegiatan
* Latency
    * Delay dari suatu response
* Throughput
    * Transaction/Second
    * Transaction = Kegiatan biasa
* Load
    * Seberapa banyak tekanan sebuah sistem bisa atasi
    * Contoh:
        * Seberapa banyak user bisa pakai secara bersamaan tanpa membakar ruangan server
* Eficiency
* Scalability
    * Seberapa besar sistem kita bisa diperbesar dan dampaknya pada performa
    * Biasanya server tersebut bersifat modular agar part2nya bisa diganti dengan mudah

### Hal yang harus dilihat ketika membuat sebuah sistem
* Scalability > capacity and efficiency
* Lebih mudah dan murah beli server baru daripada membuat algoritma baru    

### Software as a service
* Software as a service (SAAS)
* Platform as a service (PAAS)
* Infrastructure as a service (IAAS)

***