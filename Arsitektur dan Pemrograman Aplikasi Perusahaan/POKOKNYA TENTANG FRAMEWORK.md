## TENTANG PERSISTENCE
### Sebuah kelas yang akan menjadi model (yang presist ke database) memiliki hal berikut

```java
@Entity
@Table(name = "name")
public class NameModel implements Serializable {
    
    @Id // Going to be the ID of this table
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Going to auto-increment
    private long id;

    @NotNull
    @Size(max = 50) // For string like object
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull // If you wanted the attribute to be not null
    @Column(name = "fly_hour", nullable = false) // The typical
    private int flyHour;

    @OneToMany(mappedBy = "pilot", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<FlightModel> pilotFlight;
}
```

****

**@Service** digunakan untuk membuat kelas di bawahnya sebagai service yang bisa dipakai oleh framework

**@Autowired** membuat interface di bawahnya menjadi seperti kelas. Implementasi pekerjaannya dikode oleh kelas lain yang mengimplementasikan interface tersebut, dan class tersebut menggunakan @Service di atas deklarasinya.

**Model** class pada param sebuah view digunakan untuk berkomunikasi dengan template dengan .addAtrribute(string_name,object)

**${object}** yang diletakkan pada template sama seperti django {object}

**return string dari view** akan mencoba mengambil template dengan string tersebut ("namastring".html) dan render template itu. Berkomunikasi dengan Model.

```<h3 th:text="${object.attr}"></h3>``` di dalam string tersebut, silahkan tambahkan ${object.attr} yang ada di model.

```<div th:each="object : ${pilotList}"></div>``` untuk for each. Bisa di akses object di dalam div tersebut.
