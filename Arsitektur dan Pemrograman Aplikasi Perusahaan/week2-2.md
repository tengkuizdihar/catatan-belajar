Lecturer: Ibu Eva (TUMBLING DOWN TUMBLING DOWN TUMBLING DOWN)
=============================================================

* Layering
    * Techniques to break apart complicated system
    * Layered Systems
        * Higher Layer (Closer to User)
            * Experience dictated by the lower one
            * Commonly, this layer only knew about the service provided by its immediate lower
                * Layer 4 knew service of Layer 3, but doesnt know bout 2.
        * Lower Layer (Closer to developer)
            * Doesn't know anything about the higher layer
        * **How does it work**?
            1. User (you) calls a function on the upper layer
            2. This "upper layer" calls an object on the lower one
            3. Eventually the function is performed and control is returned to user
        * Usually done in one thread, sequentially
    * (one) Layer is made of classes and modules that behaves like its on the same level.
    * **BENEFIT**
        * Understand the layer without knowing the rest layer below it
        * Modularity
        * Minimize dependencies
        * Good standardization
        * Scalable to higher level of layer
    * **DOWNSIDES**
        * Change one thing on the lower one, may change things above it (like adding a feature)
        * Extra layer can harm performance
        * It's **hard** to decide **what layer to implement and their responsibilites**
    * The three layer
        * Presentation
            * Handle HTML, HTTPS, UI/UX
            * DO NOT KEEP SUBROUTINE (things that may change database and logic, like validation) IN HERE
        * Domain
            * Bussiness logic, Calculation, Validation
        * Data
            * Manage persistent data
        * HOW TO: Separate them
            * Break 3 Layers to separate classes
            * As it became more complex, divide/distribute the classes into packages
    * Variation of three layer
        * Adding "service" between presentation and Domain. Service connects to Data Mapper
        * Adding "data mapper" between domain and data
            * "mapper" is an object that sets communication between two independent object
    * Choosing where to run the layers
        * One big ass machine
            * PRO: Easy to upgrade, fix, deploy, and no worry about compabilities
            * Cons: Responsiveness and if it died, all of service is DEAD
        * Layer by Layer
            * TODO lihat presentasi