# Pengantar Keamanan Informasi

## Informasi Mata Kuliah

| Attribute   | Description                                            |
| ----------- | :----------------------------------------------------- |
| Dosen       | Pak Amril                                              |
| Email Dosen | amril.syalim@cs.ui.ac.id                               |
| Penilaian   | * UTS dan UAS masing-masing 30%<br />* Project itu 40% |

## Why TCP/IP Is Not Secure (refresher)

* Man in the middle attack
* MITMA - Reading people's request and sent

## Profesi Manager Security

* Lebih ke risk management
* Hanya perlu mengerti konsep security dan lainnya
* Planning Security
  * Mencegah dan menanggulangi musibah/kejadian
  * Membuat
    * Incidents Response Planning (ketika sudah terjadi)
    * Disaster Recovery Planning (ketika ada bencana)
* Tinggal beli produk dan implementasi orang lain (tinggal)

---

## Pengenalan Keamanan Informasi/Komputer

> Be paranoid
>
> *Pak Amril, 2019*

* Faktor yang harus dihitung ketika mengamankan komputer/informasi
  * Pihak luar yang mengancam
  * Kejadian alam
* Manajemen keparanoid-an (risk manajemen)
  * Bila risk besar, be more paranoid
  * Paranoid ada trade-off nya (semakin repot)
* Security terjadi karena dua aspek
  * Aspek luar / ancaman pihak lain
  * Aspek dalam / kerentanan sistem
* Mencari balance antara keamanan dan convenience
  * Semakin aman sistemnya, biasanya semakin repot / tidak nyaman dipakai
* Security biasanya menerapkan *best practice* dan pengalaman

### Security Model

* CIA
  * Confidentiality
    * Kerahasiaan agar tidak dilihat orang
  * Integrity
    * Agar tidak dirubah orang ketika transmit
  * Availability
    * Tidak mengakses data meskipun harusnya bisa.
    * DDOS Attack
* CNSS Model / McCumber Cube
* Policy Education Technology, ada 3 tahap
  * Policy
  * Education
    * Awareness
    * Training
    * Education
  * Technology
    * Access control to a resource
    * Encryption and password

### What Firewall Is

Membatasi koneksi luar ke dalam. Yang harus dipelajari

* TCP/IP
* Cara koneksi berjalan
* Cara koneksi dan aplikasi bekerja