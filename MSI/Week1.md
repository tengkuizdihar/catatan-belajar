# MSI

* Dosen pak Iik
* UTS lebih dari UAS (40% vs 30%)

## Tahapan Identifikasi Masalah  untuk Merancang Sistem (Refresher)

1. Identifikasi
   1. Wawancara dengan orang eksekutif
   2. Tahu proses dan akhirnya tahu aktor
2. Validasi 
   1. Dengan observasi sistem dan lingkungan
3. Normatif
   1. Apakah suatu bisnis proses berjalan secara norma/peraturan yang berlaku.
   2. Bila sesuai berjalan dengan norma, maka **seharusnya** tidak ada yang salah.
4. Mendapatkan Requirement yang benar
   1. Hanya bisa didapatkan bila sudah mengetahui business proses yang benar.
5. LANJUT KE TAHAP PERANCANGAN SISTEM INFORMASI

## Hard vs Soft Sistem (Refresher)

* Hard
  * All system's variable is predictable
    * E.G. Build System
* Soft
  * The system's variable is unpredictable, because there's human inside of it.
  * E.G. Information System

## Sistem Informasi

### Tujuan SI

* Memperbaiki kinerja dari organisasi
* Bisa diketahui dari peningkatan kinerja pegawai/individu
* Bila kinerja tidak diperbaiki, maka SI tersebut dianggap gagal
* Pengembangan SI sekarang (untuk satu organisasi) biasanya lebih dari 2 tahun

### Tantangan Profesi

* 20-30% success rate (project)
* Biaya naik hampir 2x lipat
* 31% Project got cancelled
* Only 6.2% of software project is finished on time and on budget
  * In large company, it goes from 6.2% to 9%
* 40% of project failed to achieve their business objectives within a year of going live

## Skills Of A Pro

* Trivium
  * Logic, Grammar, Rhetoric
  * Bisa berpikir logis, menyampaikannya, dan memberikan dampak (ketika menyampaikan).
* Technology
  * Hardware, software, networking, etc.
  * Tahu kulitnya dan tahu apa kegunaanya
* Business
* Systems
  * Akan muncul teknologi baru yang bisa diintegrasikan ke sistem yang sudah ada
* Ability to work alone and in teams
* Mampu bekerja di bawah tekanan
* Mempunyai
  * Leadership (ketua tim atau perusahaan)
  * KomunikasiKetajaman bisnis
  * Strategic planning
  * Influencing others/salesmanship

## How to use SCRUM

Jangan digunakan ketika mengembangkan Sistem Informasi. Gunakan SCRUM ketika masa maintenance (adaptive maintenace), bukan ketika mengembangkan sistem.