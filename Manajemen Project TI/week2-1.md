Lecturer: Bu Larastri
=====================

### Proyek IT
* Proyek harus
    * Dijalankan secara "tidak isolasi"
    * Menggunakan holistic thinking (menyeluruh) dengan konteks organisasi
        * Bila terinspirasi dari sistem lain, cari kenapa sistem tersebut menggunakan teknologi yang mereka pakai sekarang
        * Melihat alur sistem terdahulu dan mencari masalahnya
    * Mendapat bantuan dari senior managers
    * Requirement Gathering is a must


### Jawaban
* Seharusnya Tom mencari apa penyebab kampus yang dia datangi memakai laptop. Lalu mencari data dan research mengenai fungsi dan practice dari laptop untuk kampus tersebut dan menyimpulkan dari data mengenai kegunaan laptop terebut terhadap kegiatan sehari-hari di kampus. Dia tidak berpikir secara holistik, hanya snapshot.