Lecturer: Bu Larastri
=====================

## Intro
Mengenai bagaimana caranya memanage sebuah project dari segi komunikasi, skill, perencanaan waktu, dst.
Jadi PM harus yang "bawel" tetapi bagus.

****

## Penilaian
Evaluation                              | Percentage
----------                              | :--------:
Quizzes                                 | 10%
Ass. (individual)                       | 10%
Ass. (individual)                       | 10%
Ass. (group) - Report Document          | 10%
Ass. (group) - Presentation             | 5%
Discussion (post test participation)    | 5%
Mid Test                                | 25%
Final Test                              | 25%
**Total**                               | **100%**

****

## Pelajaran
* Project's Attribute (What's a project is)
  * Unique Purpose
  * Temporary
  * Requires resources from various areas
  * Have a primary customer/sponsor
  * Involves uncertainty

****

## Wide Angle
* Part 1: Phantom Blood
  * Understand the growing need for a better project management
  * Explain what project is
  * Describe PM and key elements of the PM project
