Lecturer: Pak Zainal
====================

## Intro
Jangan berisik bila bapak sedang berbicara. Hell, jangan berbicara apapun yang lebih dari dua kalimat per menit.

****

## Silabus
* Transaksi : Sebuah kegiatan yang dilakukan sering, setiap saat.

## Pelajaran
* Revolusi industri 4.0 dengan ciri-ciri

Ciri                                    | Deskripsi/Contoh
----------------------------------------| ----------------
Data yang besar (volume)                | Facebook
Data yang beragam (variety)             | Banyak user yang datanya bisa diambil dan proses
Kecepatan proses data (velocity)        | Komputer semakin canggih, begitu juga dengan teknik prosesnya
Ketidakpastian validasi data (veracity) | Berita hoax, bot, dsb.

****

* Basic type of IS (segitiga)

Tingkat | Management  | Information System            | Decision Structure
  ----  |  ---        |  ---------------              | ------------------
Top     | Strategic   | Decision Support System       | Unstructured (because the problem is quite abstract)
Middle  | Tactical    | Management Information System | Semi-structured (there's at least a manual for it)
Bottom  | Operational | Transaction Processing System | Structured (Ok just be a slave will you XDDDDDD (this is a joke obviously))

****

* SDLC = System Development Life Cycle
  * Failure rate of a lot of CB-IS (Computer Based Information System) is high, yet there's still a lot of company still exist. It's because the demand is high and the paycheck is higher. (Failure rate is >50%)
  * This "High Risk, High Reward" doesn't exist in Indonesia because the risk is high. But at least this is where we as IS under-grad could improve and make it our money field. Failure rate in Indo is >95%.
  * SIAK-NG exist because Krismon (monetary crisis) happened. To eliminate variation in standardization, cutting budgets on redundant function, and it also became a data storage.

****

* Key Person in SDLC
  * The system analyst.

****

* Planning
  * Project Init
    * Identify bussiness value
    * Identify feasibility
  * Project Management
    * Develop work plan
    * Staff it
    * Control and direct project
* Macam2 SI
    * Bussiness Process Automation
    * Bussiness Process Improvement
    * Bussiness Process Reengineering
    * Bussiness Process Opportunities   : Apa yang akan datang
* SDLC Method (Nothing is right, everything is permitted)
    * process-centered methodology, 
    * data methodology
    * object-oriented methodology