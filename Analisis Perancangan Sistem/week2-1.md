Lecturer: Pak Zainal
====================

## Intro
* PDF yang digunakan adalah PDF di week 2

## Pelajaran
* Tujuan dari sistem informasi adalah tujuan dari implementasinya
* Sistem Informasi memiliki masalahnya sendiri2, berbeda setiap kasus
* Untuk membuat sebuah IS, kita harus mengedepankan **masalah dari bisnis tersebut** dan **kesempatan bisnis** klien.
* Bertanya harus hati-hati, utamakan **identifikasi masalah** pada saat awal-awal.
    * Masalahnya di mana?
    * Bagian mana yang berpotensi turun kinerjanya (loss)? (Produksi, Marketing, dkk.)
    * Analisa juga **feasibility**
* ***Project dimulai dengan***
    * Bussiness needs
        * Because of IT/Bussiness unit
        * Because of blabbering committee
        * Recommendation of external parties
    * Sponsor project
        * The bigger the sponsor, the bigger the project.
        * Can be sponsored by an IT member.
        * Sponsor can be bussiness function (Marketting, accounting, finance, production, etc.)
    * Permulaian diakhiri dengan pembuatan **system request** yang berisi: (bisa digunakan untuk interview ke project sponsor dari 1-5)
        1.  Project Sponsor         : Orang, fungsi bisnis, dkk.
        2.  Business Need           : Kontrol, automasi, etc.
        3.  Business Requirement    : Faktor-faktor yang berperan
        4.  Business Value          : Ada penurunan 2% di operating cost, 5% lebih efisien, 50% lebih cepat, dll.
        5.  Special Issue-optional  : Bila ada sistem legacy, deadline tertentu, harus operasional di hari libur
* Feasibility Analysis but **DEEPER**
    * Dari segi (cost and benefits)
        * Technical         : Apa bisa dilakukan dengan teknik yang sekarang dimiliki atau bisa dimiliki
        * Economic          : Proyek besar tetapi budget kecil LUL
            * Tangible      : Bisa dihitung
            * Intangible    : Tidak bisa dihitung secara langsung (market share, popularitas, good name, etc.)
        * Organizational    : Demo karena ada **karyawan yang kehilangan pekerjaan**, meskipun proses sekarang semakin baik
    * Menggunakan "Feasibilities Table Studies"
* # TODO: METHOD METHOD tolong baca