Lecturer: Pak Zainal
====================

* Bila klien tidak memungkinkan untuk bertemu (wajah marah, lesu, atau tidak mendukung), sugestikan bahwa pertemuan bisa dijadwalkan ke waktu yang lain.
* Use case dari sebuah boundary (rule of thumb) ada 3 - 7. Bila sudah sampai lebih dari itu, sudah semakin pusing.
* Banyaknya laporan harus setara dengan besarnya proyek atau seberapa besar *influence* klien tersebut (pemerintah, perusahaan besar, dkk).