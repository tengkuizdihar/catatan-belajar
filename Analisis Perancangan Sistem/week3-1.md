Lecturer: Pak Zainal
====================

### Requirement ANALysis
* **System Request**
    * Aslinya lebih ke arah narasi.
    * "Project ini dibantu oleh Bapak Fulan pada tanggal blablabla"
        * Project Sponsor
    * Gunakan bahasa simpel. Project Sponsor belum tentu mengerti jargon-jargon SI
    * Hindari kata2 bahasa inggris, gunakan bahasa yang deskriptif
        * contoh:
            * "Market Share 10%" = "Dari 100 orang yang memiliki potensi untuk membeli, hanya 10 yang menjadi pembeli."
    * Di dalam dunia nyata, bila tidak ada tanda tangan/kontrak yang jelas, ide kita bisa dijual ke orang lain tanpa konsen kita (dalam proses interview)
* **Requirement Analysis**
    * Survey
        * Questioner
        * Bisa aja orangnya beda ketika ditanya, jawabannya juga.
            * Solusinya adalah dengan cara mencari waktu di mana banyak orang berkumpul, dan tanya kesepakatannya.
    * Observation
        * Checklist yang mau diamati
    * Project Scope Creep
        * Proyek bisa menjadi lebih besar scopenya daripada yang dijanjikan karena requirement yang bertambah/ditambah oleh Project Sponsor
        * Solusi: Jelaskan secara baik mengenai kotrak dan rencana/plan yang sudah ada. Lalu tanya bagaimana solusi dari requirement tambahan tersebut, sertakan juga sugesti, lalu konfirmasi.
        * JANGAN BUAT MUSUH! Jelaskan secara baik. Ini Indonesia, bukan Amerika. Gotong royong is real, so is connection.
* **Requirement Determination (not a choice below)**
    * As-Is System
        * Mengerti sistem yang sekarang ada, sebaik mungkin
    * To-Be System
        * Tunjukkan sistem baru **jauh** lebih baik daripada yang dulu
    * System Proposal
        * Proposisi yang logis mengenai SI secara fungsional dan non-fungsional
        * Diperlihatkan kepada *Approval committee*
    * Partisipasi harus ada dari Business Expert/User dan IT Analyst
        * To discover need, use this
            1. Business Process Automation
            2. Business Process Improvement
            3. Business Process Reengineering
    * Langkah:
        1. Mengerti "as-is"
        2. Information gathering
        3. Identifikasi bagian mana yang bisa diperbaiki/perbagus
        4. Buat "to-be"
* **Requirement Types**
    * Functional
        * Business Process
        * Berhubungan dengan proses dan data
        * Informasi yang harus dimiliki sebuah sistem
        * Fokus ke arah fitur dan fungsionalitas
    * Non-Functional
        * Yang membantu business process
        * Berhubungan dengan performa dan kegunaan
        * Performa, Operasional, Keamanan, Aspek budaya dan politik