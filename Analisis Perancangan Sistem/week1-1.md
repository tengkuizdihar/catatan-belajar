Lecturer: Pak Zainal
====================

## Intro
* Jangan jadi autis
* Bekerja dengan tim

## Tujuan
* Menganalisis suatu sistem dengan cara2 tertentu

## Pelajaran
* Semakin jalan waktu, semakin sedikit modal yang bisa dipakai (bila modal static)
* Kudu persuasif sebagai anak SI
* Efisien = menggunakan resource secara OPTIMAL 
* Efektif = Mencapai tujuan

## Pelajaran Ngomong | PENTING
 No | Deskripsi                                                                                         | Tujuan 
 -- | ---------                                                                                         | ------
 1. |Tanya masalahnya                                                                                   |Mengetahui Requirement
 2. |Jawab masalahnya                                                                                   |Asess kalau requirementnya memang masalah SI
 3. |Repeat dari satu sampai ada suatu dead-end atau agreement bahwa masalah tersebut memang masalah SI.|Karena kudu tahu apa ini worth my time

## Cara menggali informasi requirement
Cara      | Deskripsi
---       | ---
Checklist | infrastruktur, cara orang kerja, kebenaran informasi dengan visit ke kantornya