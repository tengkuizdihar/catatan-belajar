# PBK

## Metadata

### Dosen

* Bu Kasiah
* Pak Harry

### Pelajaran

* Bakal belajar tentang metakognisi
* Kajian learning and stuff
* Learning Theories
* Setiap kelompok akan membuat presentasi dan diskusi dengan yang lain. Seperti di MPKT dulu.
* Silabus
  * How people learn
  * Mind and brain
  * Cognitivism
  * Constructivism
  * Critical Thinking
  * Metacognition
  * Online Colaborative
  * Multimedia learning

### Kelompok - PiBiKey

| Nama                            | NPM         |
| ------------------------------- | ----------- |
| Ahmad Wigo Prasetya             | 1606878215  |
| Yaumi Alfadha                   | 17060203031 |
| Tengku Izdihar Rahman Amanullah | 1606881531  |
| Rachmat Ridwan                  | 1606886974  |
| Ihwan Edi Saputro               | 1706074934  |

Visi Kelompok: Menjadi pelopor untuk menyebrangi samudera laut ilmu yang terhampar di hadapanku.

## Pelajaran

### Pendidikan

Pendidikan di zaman modern itu adalah **sains**. Tidak boleh diduga-duga cara pembelajaran. Misalnya, *pre-existing knowledge* itu penting untuk belajar agar masa transisi dan pembiasaan lebih cepat.

### Active Learning

Kebanyakan studi yang dilakukan untuk *active learning* biasanya didasari oleh *metacognition*. Metakognisi adalah kemampuan seseorang dalam memprediksi performa mereka dalam mengerjakan sesuatu dan mengetahui seberapa *jago* mereka pada aktivitas tersebut. Key vocab untuk active learning adalah, *self assessment*, *reflection*, *metakognisi*, dan *sense-making*.

Memberikan pelajar sebuah *panggung* agar mereka dapat belajar dengan cara mereka sendiri. Tugas pengajar adalah untuk mengawasi dan memfasilitasi anak didiknya dengan cara melihat kuantitas dan kualitas, ada guru yang melihat mereka belajar apa saja, dan ada guru yang melihat apakah semua anak didiknya *fairly treated*.

Keuntungan-nya adalah,

* Dituntut untuk harus tahu
* Siswa dapat belajar dengan cara sendiri
* Memicu sifat *sadar diri* terhadap kemampuan mahasiswa pada saat itu agar mereka tahu apa lagi yang harus dipelajari

Kekurangan-nya adalah

* Guru harus mengetahui anak didiknya sudah belajar sampai mana
* Guru harus mengingatkan fokus dari pelajaran pada saat itu
* Kesempatan untuk pemahaman setiap orang bisa berbeda-beda bila tidak diawasi guru