Lecturer: Pak Bayu
==================

## Skema pengajaran
Topik | Deskripsi
---|---
Computer Network and Internet |
App layer |
Transport Layer |
Network Layer | IP address and stuff like that, TCP/IP, UDP
Link Layer |
Fundamental of data and signal | physical, tidak terlalu di bahas
Network Technology & Management | Yang keren: Security, Wireless, Backbone, Designing Network

***

## Persentase penilaian
Tugas (6 kali) 30%
Quiz (5 kali) 15%
UAS 30%
UTS 25%

***

## Lecture 1
* What is internet?
  * The wide look at it
    * Home Network > Regional ISP > Global ISP
    * Mobile Network > Global ISP
    * Institutional Network > Regional ISP
    * Regional ISP
      * Biznet, Telkom
    * Global ISP
      * DOCOMO (Jepang), AT&T (Fag)
  * Host = End System
    * Gadget, Computer, homesystem
  * Communication Links
    * Fiber Optic, Copper, Radio wave, Satellite
  * Transmission Rate = Bandwidth
  * Packet Switches = Forward packets (chunks of data)
    * Routers and switches
  * Protocols = Control sending receiving data
    * TCP, IP, HTTPS, Skype, Ethernet, etc.
  * Internet Standards
    * RFC = Request for Comments, made by Internet Engineering Task Force (IETF)
    * Need standard because people need a consistent connection between each other's network.
  * Infrastructure that provides services
    * VoIP, Web, Games, Email, Social nets, etc.
  * End systems proivdes programming interface to apps
    * Hooks that allow sending and receiving app programs to "connect" to internet
* Network Protocol
  * TCP
    1. TCP Connection Request (device)
    2. TCP Connection Response (server)
    3. Get url (device)
    4. Give file (server)
* Network Edge
  * Host is basically **clients and servers**
  * Servers often in data centers
* How to connect end system to edge router (router to ISP)
  * Residential access nets
  * Institutional access (school, hospital, company, etc.)
  * Mobile Access Network
  * Keep in mind
    * Bandwidth (bits per second) of access network
    * Shared (wifi and the like) and dedicated (XL)
  * Access net: Digital Subscriber Line (DSL)
    * Using a telephone line
    * <2,5 Mbps upstream
    * <24 Mbps downstream
    * data transfer use internet service provider, voice over DSL use telephone net
  * Access net: cable network
    * frequency division multiplexing, different channel (video, data, control) using different frequency in cable/fiber
  * Wireless Access Networks
    * Wireless LAN
      * Within Building
      * 54 Mbps transmission rate
    * Wide area wireless access
      * Provided by cellular operator
      * 10 KM
      * 1 - 10Mbps
      * 3G, 4G/LTE

PLEASE READ THE PRESENTATION AND CONVERT TO PDF AND BRING TO GITLAB. THIS NOT OVER YET.
=======================================================================================