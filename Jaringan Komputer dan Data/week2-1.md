Lecturer: Pak Ading
===================

### Alternative Core
* Circuit switching
    * Ciri2
        * Bila ada N cable, maka hanya ada N user yang bisa pakai
        * Kabel telepon jadul yang masih kudu nelpon operator
        * Paling bagus koneksinya, **tetapi** mahal dan repot maintenance
    * FDM (Frequency Division Multiplexing) vs TDM (Time Division Multiplexing)
        * FDM = Many user per instance, information seperated by frequencies
        * TDM = Only one user per instance
* Packet Switching vs Circuit Switching
    * Packet Switching
        * Bagus untuk  data yang banyak dalam waktu kecil
        * Lebih mudah di-setup (no dial-up shit)
        * Bisa macet (buffering di video contohnya)
        * Packet Delay and Loss
    * PS tapi rasa CS
        * Bandwidth besar yang stabil 
* Internet Structure
    * Network of Network
        * Banyak ISP yang sambung dengan yang lainnya dengan cara
            * Menggunakan global ISP seperti AT&T
        * Content Provider Network
            * Sebuah network yang dimiliki oleh content providernya sendiri (LINE, Whatsapp, Google, Yahoo, dkk.) agar lebih dekat ke user (low latency, bigger bandwith, etc.)
    * Layering
        * Why
            * Explicit structure for ID
                * ada standard modelnya
            * Modularization 
                * gampang disetup di berbagai platform, semacm standarisasi
                * gampang diubah layer dekat user tanpa mengubah layer di ISP dsb.
    * Internet Protocol Stack (dari atas sampai bawah)
        * Application
            * FTP, SMTP, HTTP
        * Transport
            * TCP
                * Handshake all the way babyyyyy
                * Sender want the package to be sent accurately
            * UDP
                * NO HANDSHAKE, HERE A PACKAGE, AND GOODBYE
                * Sender doesn't care about the package delivery (if its going to the destination or not)
        * Network
            * IP, Routing Protocols
        * Link
            * Ethernet, WiFi
        * Pysical
            * Bits on the wire