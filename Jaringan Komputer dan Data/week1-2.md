Lecturer: Pak Ading
===================

## Lesson

* Network Core
    * Sebuah kumpulan router yang saling terhubung
    * Memotong2 pesan dari applikasi web menjadi paket2 kecil
    * Juga meneruskan paket dari yang lain
* Packet Switching
    * Store and Forward
        * Seseorang hanya bisa menerima sebuah paket atau lebih. Kapan dia harus mengirimnya, terserah dia. Tergantung rate transfer dia
        * End To End Delay = 2 * (bits getting sent/transfer rate in bits per second)
    * Queueing Delay, Loss
        * If the packets wanted to be sent are bigger than the transfer rate then it will either
            1. The packets will queue, waiting to be sent
        * The packets are getting lost because the buffer inside router is full (cant store the packets no more)
        * Data Loss: Perspective of the receiving host (user)
        * Data Drop: Perspective of the router
* Two key network-core function
    * Routing     (when the router is thinking about packets)   : Determines source and the destination taken by packets (through LAN #1, LAN #2, Wi-Fi, etc.)
    * Forwarding  (when the router is forwarding packets)       : Move the packets from router input to the appropriate router output