Lecturer: Pak Bayu
==================

* What does an apps need
    * Data Integrity in transport layer
        * needed by apps that need lossless data
        * some apps like video/audio can tolerate some LOSS
        * delay
    * Throughput
        * require minimum amount of data input to be effective
    * Security
        * encryption