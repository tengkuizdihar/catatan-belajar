Pak Ading
=========

* DNS Name Resolution, Example
    * Doing recursively
* DNS Caching, Updating records
    * Resolve "www.yahoo.com" lalu ip yahoo disimpan di dalam cache di dalam browser
    * Bila cache sudah expire,
* DNS Protocol
    * format : (name, value, type, ttl) <<<< this one is sent from the DNS server (i think) called Resource Records
    * TTL means time to live
    * there's 4 type
        * A
            * name is the hostname
            * value is IP address
        * NS
            * name is domain
            * value is hostname of the domain
        * CNAME (canonical name)
            * name is alias of the real name
            * value is the real name
        * MX
            * value is the mailserver
            * name is the mail name of the value (**dihar**@gmail.com)
        * [And many more...](https://en.wikipedia.org/wiki/List_of_DNS_record_types)
* 